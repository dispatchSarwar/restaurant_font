import React from 'react';


class App extends React.Component {

    constructor() {
        super();
        this.state = {
            txt: '',
            title: 'React test state',
            quantity: 12
        }

    }

    update(e) {
        this.setState({
            txt: e.target.value
        })
    }

    render() {
        return (
            <div>
                <h1>{this.state.title}</h1>
                <strong>{this.state.txt}</strong>
                <p>{this.state.quantity}</p>
                <hr/>
                <TxtInputWidget update={this.update.bind(this)}/>
                <TxtInputReviews update={this.update.bind(this)}/>
            </div>
        )
    }

}

/*

App.propTypes = {
    txt: PropTypes.string.isRequired,
    title: PropTypes.string,
    quantity: PropTypes.number.isRequired
}
*/

const TxtInputWidget = (props) =>
    <input type='text' onChange={props.update}/>
const TxtInputReviews=(props)=>
    <input type="text" onChange={props.update}/>


export default App

